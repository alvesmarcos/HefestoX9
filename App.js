import React from 'react';
import {
  View,
  Text,
} from 'react-native';
import firebase from 'react-native-firebase';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import Routes from './src/navigation/Routes';
import reducers from './src/reducers';

//-- retira warnings
console.disableYellowBox = true;

const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

firebase.auth().signInAnonymously()
.then((user) => {
  console.log(user.isAnonymous);
});

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Routes />
      </Provider>
    );
  }
}

export default App;