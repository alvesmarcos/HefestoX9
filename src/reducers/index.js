import { combineReducers } from 'redux';
import PerfilReducer from './PerfilReducer';
import FeedReducer from './FeedReducer';

export default combineReducers({
  PerfilReducer,
  FeedReducer,
});