const INITIAL_STATE = {
  data: '',
  campus: '',
  centro: '',
  descricao: '',
  foto: '',
};

export default (state=INITIAL_STATE, action) => {
  switch(action.type) {
    default:
      return state;
  }
};
