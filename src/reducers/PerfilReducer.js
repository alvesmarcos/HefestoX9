import { MODIFICA_ID_PERFIL, MODIFICA_NOME_PERFIL, MODIFICA_SENHA_PERFIL, MODIFICA_EMAIL_PERFIL } from '../actions/types';

const INITIAL_STATE = {
  id: '',
  nome: '',
  email: '',
  foto: '',
  stories: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case MODIFICA_ID_PERFIL:
      return { ...state, id: action.payload };
    case MODIFICA_NOME_PERFIL:
      return { ...state, nome: action.payload };
    case MODIFICA_SENHA_PERFIL:
      return { ...state, senha: action.payload };
    case MODIFICA_EMAIL_PERFIL:
      return { ...state, email: action.payload };
    default:
      return state;
  }
};
