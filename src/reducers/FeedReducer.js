import { CARREGA_POSTAGENS } from '../actions/types';

const INITIAL_STATE = {
  postagens: [],
  usuarios: [],
};

export default (state=INITIAL_STATE, action) => {
  switch(action.type) {
    case CARREGA_POSTAGENS:
      return { ...state, postagens: action.payload.postagens, usuarios: action.payload.usuarios };
    default:
      return state;
  }
};
