import React from 'react';
import {
  View,
  Text,
  StatusBar,
  FlatList,
  Image,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import moment from 'moment';
import Ionicons from 'react-native-vector-icons/Ionicons';

class HomeScreen extends React.Component {
  getUsuario(userId) {
    const { usuarios } = this.props;

    for (let i = 0; usuarios.length; ++i) {
      if (usuarios[i].id === userId) {
        return usuarios[i];
      }
    }
  }

  renderItem(item) {
    const usuario = this.getUsuario(item.usuarioId);
    return (
      <View style={{ paddingVertical: 8 }}>
        <View style={{ flexDirection: 'row', padding: 16, alignItems: 'center', justifyContent: 'space-between' }}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image
              style={{ width: 40, height: 40, borderRadius: 20 }}
              source={{ uri: usuario.foto }}
            />
            <View>
              <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 16, paddingHorizontal: 16, color: '#424242' }}>{usuario.username}</Text>
              <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 11, paddingHorizontal: 16, }}>{item.campus}</Text>
            </View>
          </View>
          {/* <Text style={{ fontFamily: 'Roboto-Regular', }}>{String(moment(item.data, 'DD-MM-YY', 'pt-br'))}</Text> */}
        </View>
        <View style={{ borderTopColor: '#EEEEEE', borderTopWidth: 1, borderBottomColor: '#EEEEEE', borderBottomWidth: 1 }}>
          <Image
            source={{ uri: item.foto, height: 250 }}
          />
        </View>
        <View style={{ flexDirection: 'row', paddingHorizontal: 16, paddingVertical: 8 }}>
          <View style={{ marginRight: 8 }}>
            <Ionicons
              name={'ios-megaphone-outline'}
              size={30}
            />
          </View>
          <View style={{ marginLeft: 8 }}>
            <Ionicons
              name={'ios-chatbubbles-outline'}
              size={30}
            />
          </View>
          {/* <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 12, paddingHorizontal: 16, }}>{String(item.curtidas).concat(' pessoas curtiram essa cabuetagem')}</Text> */}
        </View>
        <View style={{ flexDirection: 'row' }}>
          <Text style={{ paddingHorizontal: 16, }}>
            <Text style={{ fontFamily: 'Roboto-Bold', fontSize: 16, color: '#424242' }}>{usuario.username.concat(' ')}</Text>
            <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 14, color: '#424242' }}>{item.descricao}</Text>
          </Text>
        </View>
      </View>
    );
  }

  render() {
    const { postagens } = this.props;
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <StatusBar
          backgroundColor={'#bdbdbd'}
        />
        <FlatList
          keyExtractor={item => item.id}
          data={postagens}
          renderItem={({ item }) => this.renderItem(item)}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  postagens: state.FeedReducer.postagens,
  usuarios: state.FeedReducer.usuarios,
});

export default connect(mapStateToProps, {})(HomeScreen);