import React from 'react';
import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import Hr from 'react-native-hr-plus';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { carregaPostagens } from '../../actions/FeedActions';

class LoginSetupScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      indicatorVisible: false,
    };
    //--
    const { navigate, goBack, dispatch } = this.props.navigation;
    this.nav = navigate;
    this.navBack = goBack;
    this.dispatch = dispatch;
  }

  async loadPostagens() {
    try {
      this.setState({ indicatorVisible: true });
      await this.props.carregaPostagens();

      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Home' })],
        key: null,
      });
      this.dispatch(resetAction);

    } catch (e) {
      Alert.alert(e.toString());
      this.setState({ indicatorVisible: false });
    }
  }

  render() {
    const { indicatorVisible } = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: '#66BB6A', padding: 16, justifyContent: 'center' }}>
        <StatusBar backgroundColor={'#43A047'} />
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ fontFamily: 'Rancho-Regular', fontSize: 100, color: '#fff', textAlign: 'center' }}>{'X9'}</Text>
          <Hr color='#fff' width={1}>
            <Text style={{ fontFamily: 'Roboto-Light', fontSize: 14, color: '#fff', paddingHorizontal: 16 }}>Como funciona</Text>
          </Hr>
          <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 16 }}>
            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 0.33 }}>
              <SimpleLineIcons
                name={'camera'}
                color={'#fff'}
                size={50}
              />
              <Text style={{ fontFamily: 'Rancho-Regular', fontSize: 25, color: '#fff', textAlign: 'center' }}>Capture</Text>
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 0.33 }}>
              <SimpleLineIcons
                name={'location-pin'}
                color={'#fff'}
                size={45}
              />
              <Text style={{ fontFamily: 'Rancho-Regular', fontSize: 25, color: '#fff', textAlign: 'center' }}>Descreva</Text>
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 0.33 }}>
              <SimpleLineIcons
                name={'volume-2'}
                color={'#fff'}
                size={50}
              />
              <Text style={{ fontFamily: 'Rancho-Regular', fontSize: 25, color: '#fff', textAlign: 'center' }}>Cabuete</Text>
            </View>
          </View>
        </View>
        {indicatorVisible &&
          <ActivityIndicator size={60} color={'#fff'} />
        }
        {!indicatorVisible && 
        <View style={{ flexDirection: 'column', justifyContent: 'flex-end' }}>
          <TouchableOpacity
            onPress={() => false}
            style={{ backgroundColor: '#66BB6A', marginTop: 16, borderRadius: 5, borderColor: '#fff', borderWidth: 1 }}>
            <View style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
              {/* <Zocial name='google' color='#424242' size={20} /> */}
              <Text style={{ color: '#fff', padding: 16, alignSelf: 'center', fontSize: 16, fontFamily: 'Roboto-Regular' }}>Criar Conta</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.loadPostagens()}
            style={{ backgroundColor: '#fff', marginTop: 16, borderRadius: 5 }}>
            <View style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
              {/* <Zocial name='facebook' color='#fafafa' size={20} /> */}
              <Text style={{ color: '#66BB6A', padding: 16, alignSelf: 'center', fontSize: 16, fontFamily: 'Roboto-Regular' }}>Entrar</Text>
            </View>
          </TouchableOpacity>
        </View>
        }
      </View>
    );
  }
}

export default connect(null, { carregaPostagens })(LoginSetupScreen);