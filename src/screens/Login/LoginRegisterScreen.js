import React from 'react';
import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  Image,
  ActivityIndicator,
  Alert,
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import Hr from 'react-native-hr-plus';
import { TextField } from 'react-native-material-textfield';
import { registerWithEmail, mudaNomePerfil, mudaSenhaPerfil, mudaEmailPerfil, carregaPostagens } from '../../actions/PerfilActions';

class LoginRegisterScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      indicatorVisible: false,
    };
    //--
    const { navigate, goBack, dispatch } = this.props.navigation;
    this.nav = navigate;
    this.navBack = goBack;
    this.dispatch = dispatch;
  }

  async doRegisterWithEmail() {
    try {
      this.setState({ indicatorVisible: true });
      await this.props.registerWithEmail();
      await this.props.carregaPostagens();

      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Home' })],
      });
      this.dispatch(resetAction);

    } catch (e) {
      Alert.alert(e.toString(e));
      this.setState({ indicatorVisible: false });
    }
  }

  render() {
    const { indicatorVisible } = this.state;
    const { nome, email, senha } = this.props;
    return (
      <ScrollView style={{ flexGrow: 1, backgroundColor: '#fafafa', padding: 16 }}>
        <StatusBar backgroundColor={'#bdbdbd'} />
        <View style={{ justifyContent: 'center', alignItems: 'center', marginVertical: 16 }}>
          <Image
            style={{ width: 140, height: 140, borderRadius: 70 }}
            source={require('../../assets/img/person-placeholder-male.jpg')}
          />
        </View>
        <Hr color='#bdbdbd' width={1}>
          <Text style={{ fontFamily: 'Roboto-Light', fontSize: 14, color: '#424242', paddingHorizontal: 16 }}>Seus dados</Text>
        </Hr>
        <TextField
          label='Nome'
          value={nome}
          onChangeText={(nome) => this.props.mudaNomePerfil(nome)}
        />
        <TextField
          label='E-mail'
          value={email}
          onChangeText={(email) => this.props.mudaEmailPerfil(email)}
        />
        <TextField
          label='Senha'
          value={senha}
          secureTextEntry
          onChangeText={(senha) => this.props.mudaSenhaPerfil(senha)}
        />
        {indicatorVisible &&
          <ActivityIndicator size={60} />
        }
        {!indicatorVisible &&
          <TouchableOpacity
            onPress={() => this.doRegisterWithEmail()}
            style={{ backgroundColor: '#fafafa', marginTop: 16, borderRadius: 5, borderColor: '#424242', borderWidth: 1 }}>

            {/* <Zocial name='google' color='#424242' size={20} /> */}
            <Text style={{ color: '#424242', padding: 16, alignSelf: 'center', fontSize: 16, fontFamily: 'Roboto-Regular' }}>Criar Conta</Text>
          </TouchableOpacity>
        }
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  email: state.PerfilReducer.email,
  nome: state.PerfilReducer.nome,
  senha: state.PerfilReducer.senha,
});

export default connect(mapStateToProps, { registerWithEmail, mudaEmailPerfil, mudaNomePerfil, mudaSenhaPerfil, carregaPostagens })(LoginRegisterScreen);