import React, {Component} from 'react';
import {
  ActivityIndicator,
  Dimensions,
  View,
  Text,
  HeaderBackButton,
  TouchableOpacity,
  CameraRoll,
  Modal,
  Image,
  Animated,
  Easing,
  StyleSheet,
  Platform,
  StatusBar,
} from 'react-native';

import { NavigationActions } from 'react-navigation';


import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { RNCamera } from 'react-native-camera';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
 },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 35,
    borderWidth:10,
    borderColor: 'rgba(255,255,255,0.5)',
    padding: 10,
    margin: 40,
    height: 70,
    width: 70,
},
cameraOptions: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'stretch',
    alignItems: 'flex-start',
    marginHorizontal: 16,
    marginTop: 16,
  },
  iconCameraOptions: {
    color: '#fff'
  },
  textProcessing:{
    backgroundColor: '#0b8',
    fontSize: 40,
    textAlign: 'center',
  },
  processing:{
    flex: 1,
    flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center',
    backgroundColor: '#0b8',
  },
  image: {
      width: 256,
      height: 256,
      backgroundColor: 'transparent',
  },
});


class CameraScreen extends Component {
  static navigationOptions = {
    header: null,
  }

  constructor(props) {
    super(props);
    this.state = {
      flash: 'off',
      flashIcon: false,
      type: 'back',
      path: 'default',
      visibleLoading: false,
    };

    this.RotateValueHolder = new Animated.Value(0);
  }

  componentDidMount() {
    //this.StartImageRotateFunction();
  }

  toggleFlash = () => {
    this.setState({flashIcon: !this.state.flashIcon});
    this.setState({
      flash: this.state.flash === 'off' ? 'on' : 'off',
    });
  }

  toggleFacing = () => {
    this.setState({
      type: this.state.type === 'back' ? 'front' : 'back',
    });
  }


  takePicture = async function() {
    if (this.camera) {
      const options = { quality: 0.5, base64: true };
      this.setState({visibleLoading: true});
      
      const data = await this.camera.takePictureAsync(options)
    
      // const resetAction = NavigationActions.reset({
      //   index: 0,
      //   actions: [
      //     NavigationActions.navigate({ routeName: 'Form', params: {pictureBase64: data.base64, picturePath: data.path } }),
      //  ],
      // });
      const newAction = NavigationActions.navigate({ routeName: 'Form', params: {pictureBase64: data.base64, picturePath: data.uri } });
      this.props.navigation.dispatch(newAction);
    }
    this.setState({visibleLoading: false});
  };


  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor={'#000'} />
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={this.state.type}
          flashMode={this.state.flash}
          autoFocus={RNCamera.Constants.AutoFocus.on}
          permissionDialogTitle={'Permission to use camera'}
          permissionDialogMessage={'We need your permission to use your device camera'}
          >
          <View style={styles.cameraOptions}>
            <TouchableOpacity
              onPress={this.toggleFacing}
            >
              <MaterialIcon name="switch-camera" size={30} style={styles.iconCameraOptions} />
            </TouchableOpacity>
            { this.state.flashIcon
              ? <TouchableOpacity
                  onPress={this.toggleFlash}
                >
                  <MaterialCommunityIcon name="flash" size={30} style={styles.iconCameraOptions} />
                </TouchableOpacity>
              :
                <TouchableOpacity
                  onPress={this.toggleFlash}
                >
                  <MaterialCommunityIcon name="flash-off" size={30} style={styles.iconCameraOptions} />
                </TouchableOpacity>
            }
          </View>
          <TouchableOpacity
            onPress={
              this.takePicture.bind(this)}
            style = {styles.capture}
          />
        </RNCamera>

        <Modal onRequestClose={() => null} animationType='fade' transparent={false} visible={this.state.visibleLoading} >
          <View style={styles.processing}>
            <ActivityIndicator size={120} color={'#fff'} />
          </View>
        </Modal>
      </View>
    );
  }
}

export default CameraScreen;