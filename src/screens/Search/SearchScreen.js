import React from 'react';
import {
  View,
  Text,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

class SearchScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fafafa', justifyContent: 'center', alignItems: 'center', padding: 16 }}>
        <MaterialIcons 
          name={'search'}
          color={'#bdbdbd'}
          size={130}
        />
      </View>
    );
  }
}

export default SearchScreen;