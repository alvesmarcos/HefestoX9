import React, { Component } from 'react';
import {
	Text,
	View,
	Platform,
	StyleSheet,
	ScrollView,
	TouchableOpacity,
	AsyncStorage,
	Image,
	TextInput,
	FlatList,
	Modal,
	Alert,
	StatusBar,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { enviaCabuatagem } from '../../actions/FeedActions';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: '#CED0CE',
	},

	test: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: '#FF0',
	},

	header: {
		height: (Platform.OS == 'ios') ? 70 : 50,
		paddingTop: (Platform.OS == 'ios') ? 20 : 0,
		backgroundColor: '#FFF',
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
		paddingHorizontal: 20,
		marginBottom: 5,
	},

	itemListHeader: {
		height: 50,
		paddingTop: 0,
		backgroundColor: '#FFF',
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection: 'row',
		paddingHorizontal: 10,
		marginBottom: 2,
	},

	locationButton: {
		fontSize: 15,
		marginTop: 0,
	},

	locationTouchcable: {
		borderBottomColor: '#aaa',
		borderBottomWidth: 1,
	},

	backButton: {
		fontSize: 24,
		fontWeight: 'bold',
	},

	submitArea: {
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#0B8',
		height: 51,
	},

	submitButton: {
		fontSize: 20,
		fontWeight: 'bold',
		color: '#FFF',
	},

	headerText: {
		fontSize: 16,
		fontWeight: 'bold',
	},

	locationHeader: {
		fontSize: 16,
		height: 30,
		fontWeight: 'bold',
		backgroundColor: '#FFF',
	},

	descrimg: {
		padding: 0,
		backgroundColor: '#FFF',
		height: 140,
		marginBottom: 5,
		flexDirection: 'row',
		alignItems: 'center',
		alignContent: 'flex-start',

	},

	repo: {
		padding: 20,
		backgroundColor: '#FFF',
		height: 120,
		marginBottom: 20,
		borderRadius: 5,
	},

	locationSection: {
		padding: 10,
		backgroundColor: '#FFF',
		height: 260,
		marginBottom: 5,
		flexDirection: 'column',
		alignContent: 'flex-start',
		borderBottomColor: 'white',
		borderBottomWidth: 1,
	},

	locationEntry: {
		padding: 0,
		backgroundColor: '#FFF',
		height: 60,
		marginBottom: 10,
		flexDirection: 'column',
		alignItems: 'flex-start',
		justifyContent: 'flex-start',
		paddingHorizontal: 0,
		marginBottom: 5,
	},

	locationEntryArea: {
		padding: 0,
		backgroundColor: '#FFF',
		height: 120,
		flexDirection: 'column',
		alignItems: 'flex-start',
		justifyContent: 'flex-start',
		paddingHorizontal: 0,
		paddingVertical: 0,
		marginBottom: 5,
	},

	image: {
		width: 112,
		height: 70,
	},

	description: {
		marginLeft: 0,
	},

	boxInputDescription: {
		marginTop: 10,
		paddingVertical: 0,
		paddingHorizontal: 5,
		borderWidth: 1,
		borderColor: '#DDD',
		height: 100,
		width: 190,
		textAlign: 'left',
		textAlignVertical: 'top',
		fontSize: 15,
	},

	boxInput: {
		marginTop: 0,
		paddingVertical: 0,
		paddingHorizontal: 5,
		borderWidth: 1,
		borderColor: '#DDD',
		height: 55,
		width: 280,
		textAlign: 'left',
		textAlignVertical: 'top',
		fontSize: 15,
	},

	flatList: {
		backgroundColor: '#999'
	}

});

class FormScreen extends Component {

	constructor(props) {
		super(props)

		this.state = {
			modalVisible: false,
			repos: [],
			campus: [{
				key: 0, name: 'Campus I - João Pessoa',
				centro: [{ key: 0, name: 'Centro de Ciências Exatas e da Natureza' },
				{ key: 1, name: 'Centro de Ciências Humanas, Letras e Artes' },
				{ key: 2, name: 'Centro de Ciências Médicas' },
				{ key: 3, name: 'Centro de Educação' },
				{ key: 4, name: 'Centro de Ciências Sociais Aplicadas' },
				{ key: 5, name: 'Centro de Tecnologia' },
				{ key: 6, name: 'Centro de Ciências da Saúde' },
				{ key: 7, name: 'Centro de Ciências Jurídicas' },
				{ key: 8, name: 'Centro de Biotecnologia' },
				{ key: 9, name: 'Centro de Comunicação, Turismo e Artes' },
				{ key: 10, name: 'Centro de Energias Alternativas e Renováveis' },
				{ key: 11, name: 'Centro de Informática' },
				{ key: 12, name: 'Centro de Tecnologia e Desenvolvimento Regional' }]
			},

			{
				key: 1, name: 'Campus II - Areia',
				centro: [{ key: 0, name: 'Centro de Ciências Agrárias' }]
			},

			{
				key: 2, name: 'Campus III - Bananeiras',
				centro: [{ key: 0, name: 'Centro de Ciências Humanas, Sociais e Agrárias' }]
			},

			{
				key: 3, name: 'Campus IV - Rio Tinto e Mamanguape',
				centro: [{ key: 0, name: 'Centro de Ciências Aplicadas e Educação' }]
			}],

			visibleCampus: false,
			visibleCentro: false,
			selectedCampus: 'Selecionar Campus',
			selectedCentro: 'Selecionar Centro   ',
			selectedCampusKey: 0,
			formData: { description: '', campus: '', centro: '' },
		};
	}

	static navigationOptions = {
		title: 'Cabuete',
		tabBarIcon: ({ tintColor }) => <TouchableOpacity onPress={() => this.setState({ modalVisible: true })}>
			<Text style={styles.backButton}>&lt;</Text>
		</TouchableOpacity>

	}

	async sendCabuetagem(descricao, campus, centro, local) {
		try {
			await this.props.enviaCabuatagem(this.props.navigation.state.params.picturePath, descricao, campus, centro, local);

			Alert.alert(
				'',
				'Cabuetagem enviada!',
				[
					{
						text: 'OK', onPress: () => {
							const resetAction = NavigationActions.reset({
								index: 0,
								actions: [
									NavigationActions.navigate({ routeName: 'Home' }),
								],
							});

							this.props.navigation.dispatch(resetAction);
						}
					},
				],
				{ cancelable: false }
			);

		} catch (e) {
			Alert.alert(String(e));
		}
	}

	render() {
		return (

			<ScrollView>
				<View style={styles.container}>
					<StatusBar
						backgroundColor={'#bdbdbd'}
					/>

					<View style={styles.descrimg}>
						<Image
							style={styles.image}
							source={{
								isStatic: false,
								uri: 'data:image/jpeg;base64,' + this.props.navigation.state.params.pictureBase64,
							}}
							rotation={90}
						/>
						<View style={styles.description}>
							<TextInput
								autoCapitalize='none'
								style={styles.boxInputDescription}
								underlineColorAndroid='rgba(0, 0, 0, 0)'
								placeholder='Escreva uma descrição'
								multiline={true}
								maxLength={200}
								numberOfLines={5}
								value={this.state.descriptionText}
								onChangeText={newText => this.setState({ descriptionText: newText })}
							/>
						</View>
					</View>

					<View style={styles.locationSection}>
						<Text style={styles.locationHeader}> Localização </Text>
						<View style={styles.locationEntry}>
							<Text>Campus</Text>
							<TouchableOpacity style={styles.locationTouchcable} onPress={() => { this.setState({ visibleCampus: true }); this.setState({ selectedCentro: 'Selecionar Centro   ' }) }}>
								<Text style={styles.locationButton}>{this.state.selectedCampus + Array(80 - this.state.selectedCampus.length).join(" ")}</Text>
							</TouchableOpacity>
						</View>

						<View style={styles.locationEntry}>
							<Text>Centro</Text>
							<TouchableOpacity style={styles.locationTouchcable} onPress={() => this.setState({ visibleCentro: true })}>
								<Text style={styles.locationButton}>{this.state.selectedCentro + Array(160 - this.state.selectedCampus.length).join(" ")}</Text>
							</TouchableOpacity>
						</View>

						<View style={styles.locationEntryArea}>
							<Text>Local</Text>
							<TextInput
								autoCapitalize='none'
								style={styles.boxInput}
								underlineColorAndroid='rgba(0, 0, 0, 0)'
								placeholder='Ex: sala 102, estacionamento'
								maxLength={40}
								numberOfLines={3}
								multiline={true}
								value={this.state.areaNameText}
								onChangeText={newText => this.setState({ areaNameText: newText })}
							/>
						</View>
					</View>

					<TouchableOpacity onPress={() => {
						if (this.state.selectedCampus == 'Selecionar Campus' || this.state.selectedCentro == 'Selecionar Centro   ' ||
							this.state.descriptionText == null || this.state.descriptionText == '') {
							Alert.alert(
								'',
								'Preencha todos os campos!',
								[
									{ text: 'OK', onPress: () => { } },
								],
								{ cancelable: false }
							)
							return;
						}
						this.setState({ formData: { description: this.state.descriptionText, campus: this.state.selectedCampus, centro: this.state.selectedCentro } })
						this.sendCabuetagem(this.state.descriptionText, this.state.selectedCampus, this.state.selectedCentro, this.state.areaNameText);
					}}>
						<View style={styles.submitArea}>
							<Text style={styles.submitButton}>cabuetar</Text>
						</View>
					</TouchableOpacity>

					<Modal onRequestClose={() => null} animationType='fade' transparent={false} visible={this.state.visibleCampus} >
						<View style={styles.flatList}>
							<FlatList
								data={this.state.campus}
								renderItem={({ item }) =>
									<TouchableOpacity onPress={() => {
										this.setState({ visibleCampus: false })
										this.setState({ selectedCampus: item.name })
										this.setState({ selectedCampusKey: item.key })
									}
									}
										activeOpacity={0.85}>
										<View style={styles.itemListHeader}>
											<Text style={styles.locationButton}>{item.name}</Text>
										</View>
									</TouchableOpacity>}
								keyExtractor={item => item.key}
							/>
						</View>
					</Modal>

					<Modal onRequestClose={() => null} animationType='fade' transparent={false} visible={this.state.visibleCentro}>
						<View style={styles.flatList}>
							<FlatList
								data={this.state.campus[this.state.selectedCampusKey].centro}
								renderItem={({ item }) =>
									<TouchableOpacity onPress={() => {
										this.setState({ visibleCentro: false })
										this.setState({ selectedCentro: item.name })
									}
									}
										activeOpacity={0.85}>
										<View style={styles.itemListHeader}>
											<Text style={styles.locationButton}>{item.name}</Text>
										</View>
									</TouchableOpacity>}
								keyExtractor={item => item.key}
							/>
						</View>
					</Modal>

				</View>
			</ScrollView>

		);
	}
}

export default connect(null, { enviaCabuatagem })(FormScreen);