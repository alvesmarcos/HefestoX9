import React from 'react';
import { createStackNavigator } from 'react-navigation';
import BottomNavigation from './BottomNavigation';
import { LoginStack } from './StackNavigation';
import { CameraScreen, FormScreen } from '../screens';

const Routes = createStackNavigator(
  {
    Login: { 
      screen: LoginStack,
    },
    Home: { 
      screen: BottomNavigation,
    },
    Camera: { 
      screen: CameraScreen, 
    },
    Form: {
      screen: FormScreen,
    }
  }, {
    initialRouteName: 'Login',
    navigationOptions: { header: null },
  }
);

export default Routes;