import React from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  HomeStack,
  SearchStack,
  RankingStack,
  AccountStack,
} from './StackNavigation';

const BottomNavigation = createBottomTabNavigator(
  {
    Home: { screen: HomeStack },
    Search: { screen: SearchStack },
    Ranking: { screen: RankingStack },
    Account: { screen: AccountStack },
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;

        switch (routeName) {
          case 'Home':
            iconName = 'home';
            break;
          case 'Search':
            iconName = 'search';
            break;
          case 'Ranking':
            iconName = 'star';
            break;
          case 'Account':
            iconName = 'person';
            break;
        }
        return <Icon name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'black',
      inactiveTintColor: 'gray',
      showLabel: false,
      style: {
        backgroundColor: 'white',
        elevation: 5,
        borderTopColor: 'white'
      },
    },
    animationEnabled: true,
    swipeEnabled: false,
  }
);

export default BottomNavigation;