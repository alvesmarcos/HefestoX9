import React from 'react';
import {
  View,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { createStackNavigator } from 'react-navigation';
import {
  AccountScreen,
  LoginRegisterScreen,
  LoginSetupScreen,
  HomeScreen,
  SearchScreen,
  RankingScreen,
  CameraScreen,
} from '../screens';

const HomeStack = createStackNavigator(
  {
    Home: { screen: HomeScreen },
  },
  {
    navigationOptions: ({ navigation }) => ({
      title: 'Cabuetagens',
      headerTitleStyle: {
        fontFamily: 'Roboto-Regular',
        fontWeight: 'normal',
      },
      headerTintColor: '#424242',
      headerStyle: {
        backgroundColor: '#fafafa',
      },
      headerRight: (
        <TouchableOpacity onPress={() => navigation.navigate('Camera')}>
          <Icon style={{paddingRight:23}} name='camera-alt' size={24} color={'#424242'} />
        </TouchableOpacity>
      ),
    }),
  }
);

const SearchStack = createStackNavigator(
  {
    Search: { screen: SearchScreen }
  },
  {
    navigationOptions: ({ navigation }) => ({
      title: 'Pesquisar',
      headerTitleStyle: {
        fontFamily: 'Roboto-Regular',
        fontWeight: 'normal',
      },
      headerTintColor: '#424242',
      headerStyle: {
        backgroundColor: '#fafafa',
        elevation: 5,
      },
    }),
  }
);

const RankingStack = createStackNavigator(
  {
    Ranking: { screen: RankingScreen }
  },
  {
    navigationOptions: ({ navigation }) => ({
      title: 'Ranking',
      headerTitleStyle: {
        fontFamily: 'Roboto-Regular',
        fontWeight: 'normal',
      },
      headerTintColor: '#424242',
      headerStyle: {
        backgroundColor: '#fafafa',
        elevation: 5,
      },
    }),
  }
);

const AccountStack = createStackNavigator(
  {
    Account: { screen: AccountScreen }
  },
  {
    navigationOptions: ({ navigation }) => ({
      title: 'Sua Conta',
      headerTitleStyle: {
        fontFamily: 'Roboto-Regular',
        fontWeight: 'normal',
      },
      headerTintColor: '#424242',
      headerStyle: {
        backgroundColor: '#fafafa',
        elevation: 5,
      },
    }),
  }
);

const LoginStack = createStackNavigator(
  {
    LoginSetup: { screen: LoginSetupScreen },
    LoginRegister: { screen: LoginRegisterScreen },
  },
  {
    navigationOptions: { header: null },
});

export {
  HomeStack,
  SearchStack,
  RankingStack,
  AccountStack,
  LoginStack,
};
