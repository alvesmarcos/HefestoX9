import { MODIFICA_ID_PERFIL, MODIFICA_NOME_PERFIL, MODIFICA_SENHA_PERFIL, MODIFICA_EMAIL_PERFIL } from './types';
import { references as r } from '../util';
import firebase from 'react-native-firebase';

export const mudaNomePerfil = (campo) => ({ type: MODIFICA_NOME_PERFIL, payload: campo });

export const mudaSenhaPerfil = (campo) => ({ type: MODIFICA_SENHA_PERFIL, payload: campo });

export const mudaEmailPerfil = (campo) => ({ type: MODIFICA_EMAIL_PERFIL, payload: campo });

export const registerWithEmail = () => {
  return async (dispatch, getState) => {
    const { nome, email, senha, foto } = getState().PerfilReducer;
    try {
      const usuario = await firebase.auth().createUserWithEmailAndPassword(email, senha);
      const uid = firebase.auth().currentUser.uid;
      let fotoFinal = foto;
      if (!foto) {
        fotoFinal = 'https://firebasestorage.googleapis.com/v0/b/hefestox9-b25e7.appspot.com/o/perfis%2Fperson-placeholder-male.jpg?alt=media&token=9cb49acb-3d93-4254-9ffb-087b2119c116';
      } else {
        // TODO
      }
      // --
      await firebase.database().ref(r.USUARIOS.concat(uid)).set({ id: uid, nome, email, foto: fotoFinal });

      dispatch({ type: MODIFICA_ID_PERFIL, payload: uid });
    } catch (e) {
      throw e;
    }
  };
};