import firebase from 'react-native-firebase';
import uuid from 'uuid/v1';
import { references as r } from '../util';
import { CARREGA_POSTAGENS } from './types';
import { Alert } from 'react-native';

export const carregaPostagens = () => {
  return async(dispatch) => {
    try { 
      const snapshot1 = await firebase.database().ref(r.FEED).once('value');

      const listagem1 = [];
      //-- 
      snapshot1.forEach(child => {
        listagem1.push(child.val());
      });
      const snapshot2 = await firebase.database().ref(r.USUARIOS).once('value');
      const listagem2 = [];
      //-- 
      snapshot2.forEach(child => {
        listagem2.push(child.val());
      });
      
      dispatch({ type: CARREGA_POSTAGENS, payload: { postagens: listagem1, usuarios: listagem2 } });
      //--
    } catch (e) {
      throw e;
    }
  };
};

export const enviaCabuatagem = (foto, descricao, campus, centro, local) => {
  return async(dispatch, foto, descricao, campus, centro, local) => {
    try {
      const id = uuid();
      await firebase.storage().ref(r.FEED).put(foto);
      await firebase.database().ref(r.FEED.concat(id)).set({ 
        id,
        descricao, campus, centro, local,
        comentarios: null,
        curtidas: null,
        data: String(new Date()),
        usuarioId: 'B5j7vadj1xfpDXNI8FeW36BAkqP2' // @hefesto em breve será alterada
      });
    } catch (e) {
      throw e;
    }
  };
};